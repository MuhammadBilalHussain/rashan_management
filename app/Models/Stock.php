<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use HasFactory;

    public $guarded = [];




    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function AddedBy()
    {
        return $this->belongsTo(User::class,'added_by','id');
    }
}
