<?php

namespace App\Models;
use App\Models\Product;
use App\Models\PackageProducts;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Package extends Model
{
    use HasFactory;

    public $guarded = [];




    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }


    public function packageProducts() {
        return $this->hasMany(PackageProducts::class, 'package_id','id');
    }

}
