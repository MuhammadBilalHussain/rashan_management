<?php

namespace App\Models;


use App\Models\People;
use App\Models\Package;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssignedPackage extends Model
{
    use HasFactory;
    public $guarded = [];

    public function People()
    {
        return $this->belongsTo(People::class,'people_id','id');
    }

    public function Package()
    {
        return $this->belongsTo(Package::class,'package_id','id');
    }
}

