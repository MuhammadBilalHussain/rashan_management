<?php

namespace App\Models;

use App\Models\AssignedPackage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class People extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function assignedPackages():HasMany
    {
        return $this->hasMany(AssignedPackage::class);
    }

    public function city():BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function state():BelongsTo
    {
        return $this->belongsTo(State::class);
    }
}
