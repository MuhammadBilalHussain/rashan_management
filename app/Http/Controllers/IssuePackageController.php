<?php

namespace App\Http\Controllers;

use App\Models\AssignedPackage;
use App\Models\InventoryHistory;
use App\Models\IssuePackage;
use App\Models\PackageProducts;
use App\Models\People;
use App\Models\Product;
use Illuminate\Http\Request;

class IssuePackageController extends Controller
{
    public function index()
    {
        $issuePackages = IssuePackage::with(['People','Package'])->get();
        return view('admin.issuePackage.index', compact('issuePackages'));
    }

    public function create()
    {
        return view('admin.issuePackage.create');
    }

    public function searchDPeople(Request $request)
    {
        $people = People::where('cnic', $request->cnic)->first();
        // dd($people);
        if (is_null($people)) {
            return back()->with(['error' => 'No Person registered with this CNIC']);
        }
        $AssignedPackage = AssignedPackage::with(['Package.packageProducts.product','People'])->where('people_id',$people->id)->first();
        if ($AssignedPackage) {
            $package = $AssignedPackage->Package;
            $people = $AssignedPackage->People;
            return view('admin.issuePackage.show',compact('package','people'));
        }
        return back()->with(['error' => 'No Package Assigned to this person']);
    }

    public function store(Request $request){
        $month = now()->format('m');
        $issuePackages = IssuePackage::where('people_id',$request->d_people)->whereMonth('created_at', $month)->get();

        if(count($issuePackages) > 0){
        return back()->with(['error' => 'Package is already issued in this month for this Person']);
        }
        $packageProducts = PackageProducts::where('package_id',$request->package)->get();
        foreach($packageProducts as $item){
            $product = Product::where('id',$item->product_id)->first();
            if($product->qty < $item->qty){
                return redirect('admin/issue-package')->with('error', 'Cannot issue package. Products are out of stock');
            }
            Product::where('id',$item->product_id)->decrement('qty',$item->qty);

            InventoryHistory::create([
                'product_id'=>$item->product_id,
                'qty'=>$item->qty,
                'type'=>'sub',
                'description'=>'deducted on package#'.$request->package.' issuence'
            ]);
        }
       $data = IssuePackage::create([
            'package_id'=>$request->package,
            'people_id'=>$request->d_people,
            'issue_by'=>auth()->id()
        ]);


        return redirect('admin/issued-package/'.$data->id)->with('message', 'Package Issued successfully');
    }


    public function issuePackageReport(){
        $start_date = request()->start_date;
        $end_date = request()->end_date;
        // $parsedSDate = \Carbon\Carbon::parse($start_date);
        // $parsedEDate = \Carbon\Carbon::parse($end_date);

        // dd($parsedSDate,
        // $parsedEDate);
        if($start_date && $end_date){
            $issuePackages = IssuePackage::with(['People','Package.packageProducts.product','IssueBy'])
            ->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date)->get();
            // ->get();
            // dd($issuePackages->toSql(), $issuePackages->getBindings());
        }
        else {
            $issuePackages = IssuePackage::with(['People','Package.packageProducts.product','IssueBy'])->get();
        }
        return view('admin.issue_report.index',compact('issuePackages'));
    }

    public function issuedPackage($id) {
        $issuedPackage = IssuePackage::with(['People','Package.packageProducts','IssueBy'])
            ->where('id', $id)->first();
        $people=$issuedPackage->People;
        $package=$issuedPackage->Package;
        $issuedBy = $issuedPackage->IssueBy;
        return view('admin.issuePackage.print_issued_package', compact('issuedPackage','people','package','issuedBy'));
    }

    // public function getIssuePkg(Request $request, $id)
    // {
    //     $issuePackages = IssuePackage::where('id', $id)->pluck( 'id');

    //     return response()->json($issuePackages);
    // }
}
