<?php

namespace App\Http\Controllers;

use App\Models\InventoryHistory;
use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $stocks = Stock::with(['AddedBy'])->get();
        return view('admin.stock.index', compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $products = Product::all();
        return view('admin.stock.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        foreach ($request->data as $item) {
            Stock::create([
                'product_id' => $item['product_id'],
                'qty' => $item['qty'],
                'added_by'=>auth()->id()
            ]);
            Product::where('id', $item['product_id'])->increment('qty',$item['qty']);
            InventoryHistory::create([
                'product_id'=>$item['product_id'],
                'qty'=>$item['qty'],
                'type'=>'add',
                'description'=>'Stock Added by '.auth()->user()->first_name
            ]);
        }
        return redirect(route('admin.stock.index'))->with(['success'=>'Stock Added Successfully']);

    }

    /**
     * Display the specified resource.
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Stock $stock)
    {
        //
    }
}
