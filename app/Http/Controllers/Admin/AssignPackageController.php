<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AssignedPackage;
use App\Models\package;
use App\Models\People;
use Illuminate\Http\Request;

class AssignPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $assignPackages = AssignedPackage::all();
        $peoples = People::all();
        $packages = package::all();

        // return view('admin.assignPackage.index',
        // compact('assignPackages','packages','peoples'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
