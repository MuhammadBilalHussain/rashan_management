<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function getCities(Request $request, $state)
    {
        $cities = City::where('state_id', $state)->pluck('name', 'id');
        // dd($cities);
        return response()->json($cities);
    }
}
