<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\DonorDonation;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;

class DonarController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $donars = User::with('roles')->get(); // Eager load roles for each user
        return view('admin.Donar.index', compact('donars'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $donars = User::all();
        $roles = Role::where('name', '!=', 'Donor')->get();
        return view('admin.Donar.create', compact('donars', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     */

    public function store(Request $request)
    {

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            // 'role' => 'required|exists:roles,id',
        ]);

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request['password']),



        ]);


        $role = Role::findById($request['role_id']);

        if ($role) {
            $user->assignRole($role);
        } else {
        }


        // $role  = Role::where('name','Admin')->first();
        // $user->assignRole([$role->id]);

        return redirect(route('admin.donar.index'))->with(['success', 'Donor added successfully']);
    }


    public function storeDonor(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $user_id = $user->id;


        $role = Role::where('name', 'Donor')->first();

        if ($role) {
            $user->assignRole($role);
        }

        return redirect(url('admin/donor-list'))->with(['message' => 'Donor added successfully']);
    }

    public function donorHistory($id){
        $donorDonations = DonorDonation::where('user_id',$id)->get();
        $donorUser = User::where('id',$id)->first();
        return view('admin.rightDonor.donor_history',compact('donorDonations','donorUser'));

    }

    public function newDonation(Request $request){
        foreach ($request->data as $key => $item) {

            if (isset($item['product_name'])) {
                DonorDonation::create([
                    'user_id' => $request['user_id'],
                    'product_name' => $item['product_name'],
                    'qty' => $item['qty'],
                    'unit' => $item['unit']
                ]);
            }
        }

        return back()->with(['message' => 'Donation added successfully']);

    }


    public function donorall()
    {
        $donars = User::whereHas('roles', function ($q) {
            $q->where('name', 'Donor');
        })->get();

        return view('admin.rightDonor.index', compact('donars'));
    }

    public function createDonor(){

        $donation = DonorDonation::all();

        return view('admin.rightDonor.create', compact('donation'));
    }

    public function editDonor($id){

        $data['user'] = User::where('id', $id)->first();
       $data['donars'] = DonorDonation::where('user_id',$id)->get();

        return view('admin.rightDonor.edit')->with($data);
    }



    public function updateDonor(Request $request, $id)
{
    // return ($request->all());
    $request->validate([
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email',
    ]);

    $user = User::find($id);

    if (!$user) {
        return redirect()->back()->with(['error' => 'Donor not found']);
    }

    $user->update([
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'email' => $request->email,
    ]);

    if (isset($request->data) && is_array($request->data)) {
        foreach ($request->data as $key => $item) {
            if (isset($item['product_name'])) {
                DonorDonation::updateOrCreate(
                    ['user_id' => $id,
                     'product_name' => $item['product_name']],
                    ['qty' => $item['qty'],
                     'unit' => $item['unit']]
                );
            }
        }
    }

    return redirect(url('admin/donor-list'))->with(['message' => 'Donor updated successfully']);
}

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $donars = User::where('id', $id)->first();
        $roles = Role::all();
        return view('admin.Donar.edit', compact('donars', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     */

    public function update(Request $request, string $id)
    {
        $user = User::findOrFail($id);
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            // 'role' => 'required|exists:roles,id',
        ]);

        User::where('id', $id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request['password']),



        ]);


        $role = Role::findById($request['role_id']);

        if ($role) {
            $user->syncRoles([$role]); // Use syncRoles to update the user's roles
        } else {
        }


        // $role  = Role::where('name','Admin')->first();
        // $user->assignRole([$role->id]);

        return redirect(route('admin.donar.index'))->with(['success', 'Donor added successfully']);
    }

    /**
     * Remove the specified resource from storage.
     */

    public function destroy(string $id)
    {
        // return 123;
        $donars = User::where('id', $id);

        $donars->delete();

        return redirect()->route('admin.user.index')->with('message', 'User deleted successfully');
    }
    public function destroyDonor(string $id)
    {
        // return 123;
        $donars = User::where('id', $id);

        $donars->delete();

        return redirect(url('admin/donor-list'))->with('message', 'Donor deleted successfully');
    }


}
