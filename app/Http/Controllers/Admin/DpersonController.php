<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\State;
use App\Models\People;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;
use App\Http\Controllers\Controller;

class DpersonController extends Controller
{
    public function index(){

        $dpersons = People::with('city', 'state')->get();

        return view('admin.Dperson.index', compact('dpersons'));
    }


    public function create(){

        $states = State::all();

        return view('admin.Dperson.create', compact('states'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'father_name' => 'required',
            'gender' => 'required',
            'cnic' => ['required','numeric', 'digits:13', 'string', Rule::unique('people', 'cnic')],
            'martial_status' => 'required',
            'dob' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            // 'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'state_id' => 'required',
            'city_id' => 'required',
        ]);

        $data = [
            'name' => $request->name,
            'father_name' => $request->father_name,
            'gender' => $request->gender,
            'cnic' => $request->cnic,
            'martial_status' => $request->martial_status,
            'dob' => $request->dob,
            'mobile' => $request->mobile,
            'address' => $request->address,
            'image' => $request->image,
            'state_id' => $request->state_id,
            'city_id' => $request->city_id,
        ];

        // Upload the image and get its path
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            if ($file->isValid()) {

                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move(public_path('images/'), $filename);
                $data['image'] = '../images/' . $filename;

            } else {
                // Debugging: Check if the file is valid
                dd('Invalid File');
            }
        } else {
            $data['image'] = null; // No image provided
        }


        People::create($data);
        return redirect('admin/dperson')->with('message', 'Data added successfully');
    }


    public function edit($id){

        $person = People::where('id', $id)->first();
        $states = State::all();
        $cities = City::all();
        return view('admin.Dperson.edit', compact('person', 'states', 'cities'));
    }

    public function update(Request $request, $id){

        $request->validate([
            'name' => 'required',
            'father_name' => 'required',
            'gender' => 'required',
            'cnic' => 'required',
            'martial_status' => 'required',
            'dob' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            // 'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $person = People::find($id);

        if (!$person) {

            return redirect('admin/dperson')->with('error', 'Person not found');
        }

        if ($person->image) {
            $oldImagePath = public_path($person->image);
            if (file_exists($oldImagePath)) {
                unlink($oldImagePath);
            }
        }

        $data = [
            'name' => $request->name,
            'father_name' => $request->father_name,
            'gender' => $request->gender,
            'cnic' => $request->cnic,
            'martial_status' => $request->martial_status,
            'dob' => $request->dob,
            'mobile' => $request->mobile,
            'address' => $request->address,
            'state_id' => $request->state_id,
            'city_id' => $request->city_id,
        ];

        // Upload the new image and get its path if provided
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            if ($file->isValid()) {
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move(public_path('images/'), $filename);
                $data['image'] = '../images/' . $filename;
            } else {

                return redirect('admin/dperson')->with('error', 'Invalid Image File');
            }
        }

        $person->update($data);

        return redirect('admin/dperson')->with('message', 'Data updated successfully');
    }


    public function destroy($id){

       $person = People::where('id', $id);

       $person->delete();

       return redirect('admin/dperson')->with('message','Data deleted successfully');



    }
}
