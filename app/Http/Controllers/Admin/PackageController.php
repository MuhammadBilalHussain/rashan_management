<?php

namespace App\Http\Controllers\Admin;

use App\Models\People;
use App\Models\Package;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\AssignedPackage;
use App\Models\PackageProducts;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $packages = Package::with('product')->get();

        return view('admin.Packages.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data['package'] = Package::all();
        $data['products'] = Product::all();
        return view('admin.Packages.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'package_name' => 'required',

        ]);

        $pkg = Package::create([
            'package_name' => $request->package_name,
            'description' => $request->description,
        ]);

        foreach ($request->data as $key => $item) {
            $product = Product::where('id', $item['product_id'])->first();
            // dd($product,$item['product_id']);
            PackageProducts::create([
                'package_id' => $pkg->id,
                'product_id' =>  $product->id,
                'qty' => $item['qty'],
                'unit' => $product->unit
            ]);
        }

        return redirect('admin/package')->with('message', 'Package added successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {

        $data['package'] = Package::find($id);
        $data['products'] = Product::all();
        $data['pkgproducts'] = PackageProducts::where('package_id', $id)->get();
        // dd($data);
        return view('admin.Packages.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     */

//     public function update(Request $request, $id)
// {
//     $pkg = Package::find($id);

//     if ($pkg) {
//         $pkg->update([
//             'package_name' => $request->package_name,
//             'description' => $request->description,
//         ]);

//         if (isset($request->data) && is_array($request->data)) {
//             foreach ($request->data as $key => $item) {
//                 $product = Product::where('id', $item['product_id'])->first();
//                 // dd($product,$item['product_id']);
//                 PackageProducts::create([
//                     'package_id' => $pkg->id,
//                     'product_id' =>  $product->id,
//                     'qty' => $item['qty'],
//                     'unit' => $product->unit
//                 ]);
//             }
//         }
//     }

    // return redirect('admin/package')->with('message', 'Package updated successfully');
// }


public function update(Request $request, $id)
{
    $pkg = Package::find($id);

    if ($pkg) {
        $pkg->update([
            'package_name' => $request->package_name,
            'description' => $request->description,
        ]);

        // Delete existing entries if data is provided
        if (isset($request->data)) {
            PackageProducts::where('package_id', $pkg->id)->delete();
        }

        if (isset($request->data) && is_array($request->data)) {
            $createdPackageProducts = [];

            foreach ($request->data as $key => $item) {
                $product = Product::where('id', $item['product_id'])->first();

                if ($product) {
                    $packageProduct = PackageProducts::create([
                        'package_id' => $pkg->id,
                        'product_id' =>  $product->id,
                        'qty' => $item['qty'],
                        'unit' => $product->unit
                    ]);

                    // Add the created package product to the array
                    $createdPackageProducts[] = [
                        'package_id' => $packageProduct->package_id,
                        'product_id' => $packageProduct->product_id,
                        'unit' => $packageProduct->unit,
                    ];
                }
            }

        }
    }

    return redirect('admin/package')->with('message', 'Package updated successfully');
}


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $package = Package::where('id', $id);

        $package->delete();

        return redirect('admin/package')->with('message', 'Data deleted successfully');
    }

    public function allAssignPackage()
    {
        $packages = AssignedPackage::all();
        return view('admin.Packages.assigned_packages_list', compact('packages'));
    }

    public function assignPackage()
    {

        $d_people = People::all();
        $packages = Package::all();
        return view('admin.Packages.assign_package', compact('d_people', 'packages'));
    }


    public function PackageDetails()
    {
        $package = Package::with(['packageProducts.product'])->where('id', request()->id)->first();
        return response()->json(['data' => $package]);
    }

    // public function assignPackageNow(Request $request)
    // {
    //       $request->validate([
    //         'package' => 'required',
    //         'd_people' => 'required',

    //     ]);

    //     AssignedPackage::Create([
    //         'package_id' => $request->package,
    //         'people_id' => $request->d_people,
    //         'assigned_by' => auth()->id()
    //     ]);

    //     return redirect('admin/all-assign-package')->with('message', 'Package Assigned successfully');
    // }

    public function assignPackageNow(Request $request)
    {
        $request->validate([
            'package' => 'required',
            'd_people' => 'required',
        ]);

        $packageId = $request->package;
        $peopleId = $request->d_people;

        $existingRecord = AssignedPackage::where('people_id', $peopleId)->first();

        if ($existingRecord) {
            // Update the existing record
            $existingRecord->update([
                'package_id' => $packageId,
                'assigned_by' => auth()->id(),
            ]);
        } else {
            // Create a new record
            AssignedPackage::create([
                'package_id' => $packageId,
                'people_id' => $peopleId,
                'assigned_by' => auth()->id(),
            ]);
        }

        return redirect('admin/all-assign-package')->with('message', 'Package Assigned successfully');
    }


    public function editAssignPackage($id)
    {

        $d_people = People::all();
        $packages = Package::all();
        $assign = AssignedPackage::where(['id' => $id])->first();


        return view('admin.Packages.edit_assign_package', compact('d_people', 'packages', 'assign'));
    }


    public function editPackageDetails()
    {
        $package = Package::with(['packageProducts.product'])->find(request()->id);

        if (!$package) {
            return response()->json(['error' => 'Package not found'], 404);
        }

        return response()->json(['data' => $package]);
    }

    public function updatePackageDetails(Request $request, $id)
    {
        $request->validate([
            'package' => 'required',
            'd_people' => 'required',

        ]);
        $assignedPackage = AssignedPackage::findOrFail($id);
        //For update the record
        $assignedPackage->update([
            'package_id' => $request->package,
            'people_id' => $request->d_people,
            'assigned_by' => auth()->id()
        ]);

        return redirect('admin/all-assign-package')->with('message', 'Assigned Package updated successfully');
    }



    public function destroyAssignedPackage($id)
    {
        $package = AssignedPackage::where('id', $id);

        $package->delete();

        return redirect('admin/all-assign-package')->with('message', 'Assigned Package deleted successfully');
    }
}
