<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\InventoryHistory;
use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.inventory.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     */

     public function Show(Request $request)
    {

    }

    public function create()
    {
        $products = Product::all();
        return view('admin.inventory.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_name' => 'required',
            'qty' => 'required | numeric',
            'unit' => 'required',
        ]);
        $product = Product::create($request->except('_token'));
        Stock::create([
            'product_id'=>$product->id,
            'qty'=>$product->qty
        ]);

        return redirect(route('admin.product.index'))->with(['success'=>'Product Added Successfully']);
    }


    public function inventory_report() {
        $inventory_history = InventoryHistory::all();
        return view('admin.inventory_report.index',compact('inventory_history'));
    }

    public function edit($id)
    {

        $product = Product::find($id);
        return view('admin.inventory.edit',compact('product'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'product_name' => 'required',
            'qty' => 'required | numeric',
            'unit' => 'required',
        ]);

        $product = Product::where('id',$id)->update($request->except('_token','_method'));

        return redirect(route('admin.product.index'))->with(['success'=>'Product Update Successfully']);
    }

    public function destroy($id)
    {

        $product = Product::find($id);


        if (!$product) {
            return redirect()->route('admin.inventory.index')->with('error', 'Product not found');
        }

        $product->delete();

        return redirect(route('admin.product.index'))->with('success', 'Product deleted successfully');
    }
}
