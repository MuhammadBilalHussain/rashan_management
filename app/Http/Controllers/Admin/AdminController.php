<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\IssuePackage;
use App\Models\Package;
use App\Models\People;

class AdminController extends Controller
{
    public function AdminDashboard(){
        $users = User::all();
        $people = People::all();
        $packages = Package::all();
        $issuePkg = IssuePackage::all();
        return view('admin.dashboard',
        compact('users', 'people',
         'packages', 'issuePkg'));
    }

    public function UsersListing(){

        $userslistings = User::all();

        return view('admin.UsersListing.index',compact('userslistings'));
    }

    public function destroy($id){

        $userslisting = User::where('id', $id);

        $userslisting->delete();

        return redirect('userlist')->with('message','User deleted successfully');

    }

    public function updateUserStatus(Request $request){

        $userslisting = User::find($request->id);
        if(isset($userslisting) && $userslisting->status == 1)
        {
            $userslisting->status = 0;
        }
        else
        {
            $userslisting->status = 1;
        }
        $userslisting->save();

        return redirect()->back();
    }
}
