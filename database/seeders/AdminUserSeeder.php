<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = User::create([
            'first_name'=>'Admin',
            'last_name'=>'User',
            'email'=>'admin@gmail.com',
            'status' => 1,
            'password'=>bcrypt(12345678)
        ]);

        $role           = Role::where('name','Admin')->first();

        $permissions    = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);
    }
}
