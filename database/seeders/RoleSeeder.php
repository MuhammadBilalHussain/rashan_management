<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::insert([
            [
                'name'=>'Admin',
                'guard_name'=>'web'
            ],
            [
                'name'=>'Issuer',
                'guard_name'=>'web'
            ],
            [
                'name'=>'Manager',
                'guard_name'=>'web'
            ],
            [
                'name'=>'Donor',
                'guard_name'=>'web'
            ]
        ]);
    }
}
