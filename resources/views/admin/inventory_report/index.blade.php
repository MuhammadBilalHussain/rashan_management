@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h3>Inventory Report</h3>

                        {{-- <a href="{{ route('admin.product.create') }}" class="btn btn-primary btn-sm float-end">Add Invntory</a> --}}
                    </div>
                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">

                            <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Type</th>
                                <th>Description</th>
                                <th>Created Date</th>
                            </thead>
                            <tbody>
                                @foreach ($inventory_history as $inventory)
                                    @if (count($inventory_history) > 0)
                                        <tr>
                                            <td>{{ $inventory->id }}</td>
                                            <td>{{ @$inventory->Product->product_name }}</td>
                                            <td>{{ $inventory->qty }}</td>
                                            <td>{{ $inventory->type }}</td>
                                            <td>{{ $inventory->description }}</td>
                                            <td>{{ $inventory->created_at }}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="5">No Date Found</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
