@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
           <div class="container bg-white">
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Edit Product</div>

                <div class="ms-auto">

                </div>
            </div>
            <!--end breadcrumb-->
            <form action="{{route('admin.product.update', $product->id)}}" method="POST">
                @method('put')
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <label for="name">Name</label>
                        <input type="text" name="product_name" id="name" class="form-control" value="{{$product->product_name}}" required/>
                    </div>
                    <div class="col-md-6">
                        <label for="qty">Starting Qty</label>
                        <input type="number" name="qty" id="qty" class="form-control" value="{{$product->qty}}" required/>
                    </div>
                    <div class="col-md-6">
                        <label for="unit">Unit</label>
                        <input type="text" name="unit" id="unit" class="form-control" value="{{$product->unit}}" required/>
                    </div>

                </div>
                <button class="btn btn-primary mt-3">Update</button>
            </form>
           </div>
        </div>
    </div>
@endsection
