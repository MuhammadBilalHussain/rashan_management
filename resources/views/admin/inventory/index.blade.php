@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h3>Inventory</h3>
                        <div><a href="{{ route('admin.product.create') }}" class="btn btn-primary btn-sm">Add Inventory</a>
                            <a href="{{ route('admin.stock.create') }}" class="btn btn-primary btn-sm">Add Stock</a></div>

                    </div>

                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">
							
                            <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Unit</th>
                                <th>Current Qty</th>
                                <th > Action</th>
                            </thead>

                            <tbody>

                                @forelse ($products as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->product_name }}</td>

                                        <td>{{ $product->unit }}</td>
                                        <td>{{ $product->qty }}</td>
                                        <td>
                                            <a href="{{ route('admin.product.edit', $product->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                            <div class="btn-group-vertical">

                                                <form action="{{ route('admin.product.destroy', $product->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" onclick="return confirm('Are you sure, you want to delete this data?')"
                                                    class="btn btn-danger btn-sm">Delete</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Date Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div></div>
                </div>
            </div>
        </div>
    </div>
@endsection
