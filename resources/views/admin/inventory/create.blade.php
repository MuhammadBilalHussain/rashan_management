@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">

           <div class="container bg-white">
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Add Product</div>


            </div>
            <!--end breadcrumb-->
            <form action="{{route('admin.product.store')}}" method="POST">
@csrf
                <div class="row">
                    <div class="col-md-6">
                        <label for="name">Name</label>
                        <input type="text" name="product_name" value="{{old('product_name')}}" id="name" class="form-control" />
                    </div>
                    <div class="col-md-6">
                        <label for="qty">Starting Qty</label>
                        <input type="number" name="qty" id="qty" value="{{old('qty')}}" class="form-control" />
                    </div>
                    <div class="col-md-6">
                        <label for="unit">Unit</label>
                        <input type="text" name="unit" id="unit" value="{{old('unit')}}" class="form-control" />
                    </div>

                </div>
                <button class="btn btn-primary mt-3">Submit</button>
            </form>
           </div>
        </div>
    </div>
@endsection
