@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Issued Packages Report</h3>
                        <form action="">
                        <div class="row">
                                <div class="col-md-5">
                                    <label for="start_date">Start Date</label>
                                    <input type="date" class="form-control" name="start_date" value="{{request()->start_date}}"/>
                                </div>
                                <div class="col-md-5">
                                    <label for="end_date">End Date</label>
                                    <input type="date" class="form-control" name="end_date" value="{{request()->end_date}}"/>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary mt-4">
                                        Filter
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">
								
                            <thead>
                                <th>ID</th>
                                <th>Person Name</th>
                                <th>CNIC</th>
                                <th>Package Name</th>                              
                                <th>Package Items Details</th>
                                <th>Issue Date</th>
                                <th>Issue By</th>
                            </thead>

                            <tbody>

                                @forelse ($issuePackages as $package)
                                    <tr>
                                        <td>{{ $package->id }}</td>
                                        <td>{{ $package->People->name }}</td>
                                        <td>{{ $package->People->cnic }}</td>
                                        <td>{{ $package->Package->package_name }}</td>
                                       
                                        
                                        <td>
                                            @foreach ($package->Package->packageProducts as $pkgProduct)
                                                <span>{{$pkgProduct->product->product_name}}-{{$pkgProduct->qty}}{{$pkgProduct->product->unit}}</span>,
                                            @endforeach
                                        </td>
                                        <td>{{ $package->created_at }}</td>
                                        <td>{{ $package->IssueBy->first_name . ' '. $package->IssueBy->last_name }}</td>
                                        
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7" class="text-center">No package Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
