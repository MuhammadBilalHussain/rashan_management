@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Edit Donor</div>
                   
                    <div class="ms-auto">
                        <div class="btn-group">
                            <a href="{{ route('admin.donar.index') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row bg-white">
                    <form action="{{ url('admin/donar-update/' . $user->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div id="box">
                            @foreach ($donars as $key => $data)

                                    <div class="row">
                                        <div class="col-xs-4 col-sm-4 col-md-4 mt-2">
                                            <strong>Product Name:</strong>
                                            <input name="data[{{$key}}][product_name]" id="productName" class="form-control"
                                                value="{{ $data->product_name }}" required>
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 mt-2">

                                            <strong>Quantity:</strong>
                                            <input type="number" class="form-control" name="data[{{$key}}][qty]"
                                            value="{{$data->qty}}"/>

                                        </div>

                                        <div class="col-xs-3 col-sm-3 col-md-3 mt-2">

                                            <strong>Unit:</strong>
                                            <input type="text" class="form-control" name="data[{{$key}}][unit]"
                                            value="{{$data->unit}}"/>

                                        </div>
                                        <div class=" col-sm-2 col-md-2 mt-2">
                                            <button type="submit" id="addProduct"
                                                class="btn btn-primary mt-3 ">Add</button>

                                        </div>

                                    </div>

                            @endforeach
                        </div>
                        <div class="row">

                            <div class="col-xs- col-sm-6 col-md-6 mt-2">
                                <strong>First Name:</strong>
                                <input type="text" name="first_name" id="name"

                                value="{{$user->first_name}}"  class="form-control" />
                            </div>

                            <div class="col-xs- col-sm-6 col-md-6 mt-2">
                                <strong>Last Name:</strong>
                                <input type="text" name="last_name" id="name" value="{{ $user->last_name }}"
                                    class="form-control" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs- col-sm-6 col-md-6 mt-2">

                                <strong>Email:</strong>
                                <input type="email" class="form-control" name="email" value="{{ $user->email }}"
                                    class="form-control" />
                            </div>
                        </div>

                        <div class=" col-sm-12 col-md-12 text-end">
                            <button type="submit" class="btn btn-primary mt-2">Update</button>
                        </div>

                </div>
                </form>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection

@push('scripts')

    <script>
        $(document).ready(function() {
            $("#addProduct").on("click", function(e) {
                e.preventDefault();
                var element = document.getElementById("box");
                var numberOfChildren = element.children.length;

                let index = numberOfChildren;
                var newTask = `
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 mt-2">
                                    <strong>Product Name:</strong>
                                    <input name="data[${index}][product_name]" id="productName" class="form-control" required />


                                 </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 mt-2">
                                    <strong>Quantity:</strong>
                                    <input type="number" class="form-control" name="data[${index}][qty]" />
                                </div>

                                <div class="col-xs-3 col-sm-3 col-md-3 mt-2">
                                    <strong>Unit:</strong>
                                    <input type="text" class="form-control" name="data[${index}][unit]" />
                                </div>
                                <div class=" col-sm-2 col-md-2 mt-2">
                                    <button type="submit" id="remove_row" class="btn btn-danger btn-sm mt-3 ">Remove</button>

                                </div>

                            </div>
                    `;
                $("#box").append(newTask);


            });


            $(document).on("click", "#remove_row", function() {

                $(this).closest(".row").remove();
            })

        });
    </script>
@endpush
