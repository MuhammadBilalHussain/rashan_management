@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h3>Donor</h3>
                        <a href="{{ url('admin/donor-create') }}" class="btn btn-primary btn-sm float-end">Add Donor</a>
                    </div>

                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">
								
                            <thead>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th> Action</th>
                            </thead>

                            <tbody>

                                @forelse ($donars as $donar)
                                    <tr>
                                        <td>{{ $donar->id }}</td>

                                        <td>{{ $donar->first_name }}</td>
                                        <td>{{ $donar->last_name }}</td>
                                        <td>{{ $donar->email }}</td>
                                        <td>
                                            @if($donar->status === 1)
                                            <span class="text-primary">Active</span>
                                            @else
                                            <span class="text-danger">Inactive</span>
                                            @endif
                                        </td>
                                      <td>
                                        <a href="{{ url('admin/donor-edit/'.$donar->id) }}"
                                            class="btn btn-primary btn-sm">Edit</a>

                                            <a href="{{ url('admin/delete-donor/'.$donar->id) }}"
                                                onclick="return confirm('Are you sure, you want to delete this data?')"
                                                class="btn btn-danger btn-sm">Delete</a>
                                                <a href="{{url('admin/donar-history/'.$donar->id)}}" class="btn btn-primary btn-sm">View History</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Donar Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    {{-- <script src="https://code.jquery.com/jquery-3.7.0.js"></script> --}}
    <!-- Bootstrap JS -->
	
	<script>
		$(document).ready(function () {
			//Default data table
			$('#example').DataTable();
			
			var table = $('#example2').DataTable({
				lengthChange: false,
				buttons: ['copy', 'excel', 'pdf', 'print', 'colvis']
			});
			table.buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
		});
	</script>
	<!-- App JS -->
	
@endpush
