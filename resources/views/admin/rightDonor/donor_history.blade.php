@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <strong>Donor Name: {{ $donorUser->first_name . ' ' . $donorUser->last_name }}</strong>
                        <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#exampleModal">Add Donation</button>

                    </div>

                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">
								
                            <thead>
                                <th>ID</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Unit</th>
                            </thead>

                            <tbody>

                                @forelse ($donorDonations as $donar)
                                    <tr>
                                        <td>{{ $donar->id }}</td>

                                        <td>{{ $donar->product_name }}</td>
                                        <td>{{ $donar->qty }}</td>
                                        <td>{{ $donar->unit }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Donar Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div></div>
                </div>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Donation</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row bg-white">
                                <form action="{{ url('admin/new-donation') }}" method="POST">
                                    @csrf
                                    <input type="hidden" class="form-control" name="user_id"
                                        value="{{ $donorUser->id }}" />
                                    <div id="box">

                                        <div class="row">
                                            <div class="col-xs-4 col-sm-4 col-md-4 mt-2">
                                                <strong>Product Name:</strong>
                                                <input name="data[0][product_name]" id="productName" class="form-control"
                                                    required>

                                            </div>
                                            <div class="col-xs-3 col-sm-3 col-md-3 mt-2">

                                                <strong>Quantity:</strong>
                                                <input type="number" class="form-control" name="data[0][qty]" />

                                            </div>

                                            <div class="col-xs-3 col-sm-3 col-md-3 mt-2">

                                                <strong>Unit:</strong>
                                                <input type="text" class="form-control" name="data[0][unit]" />

                                            </div>
                                            <div class=" col-sm-2 col-md-2 mt-2">
                                                <button type="button" id="addProduct"
                                                    class="btn btn-primary mt-3 ">+</button>

                                            </div>

                                        </div>

                                    </div>

                                    <div class=" col-sm-12 col-md-12 text-end mt-5">
                                        <button type="submit" class="btn btn-primary mt-2">Submit</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $("#addProduct").on("click", function(e) {
                e.preventDefault();
                var element = document.getElementById("box");
                var numberOfChildren = element.children.length;

                let index = numberOfChildren;
                var newTask = `
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4 mt-2">
                                <strong>Product Name:</strong>
                                <input name="data[${index}][product_name]" id="productName" class="form-control" required />


                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 mt-2">
                                <strong>Quantity:</strong>
                                <input type="number" class="form-control" name="data[${index}][qty]" />
                            </div>

                            <div class="col-xs-3 col-sm-3 col-md-3 mt-2">
                                <strong>Unit:</strong>
                                <input type="text" class="form-control" name="data[${index}][unit]" />
                            </div>
                            <div class=" col-sm-2 col-md-2 mt-2">
                                <button type="button" id="remove_row" class="btn btn-danger btn-sm mt-3 ">-</button>

                            </div>

                        </div>
                `;
                $("#box").append(newTask);


            });


            $(document).on("click", "#remove_row", function() {

                $(this).closest(".row").remove();
            })

        });
    </script>
@endpush
@push('scripts')
    {{-- <script src="https://code.jquery.com/jquery-3.7.0.js"></script> --}}
    <!-- Bootstrap JS -->
	
	<script>
		$(document).ready(function () {
			//Default data table
			$('#example').DataTable();
			
			var table = $('#example2').DataTable({
				lengthChange: false,
				buttons: ['copy', 'excel', 'pdf', 'print', 'colvis']
			});
			table.buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
		});
	</script>
	<!-- App JS -->
	
@endpush
