@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Add New Package</div>
                    <div class="ps-3">
                        
                    </div>
                    <div class="ms-auto">
                        <div class="btn-group">
                            <a href="{{ url('admin/package') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row bg-white">
                    <form action="{{ url('admin.assignPackage.store') }}" method="POST">
                        @csrf
                        <div id="box">

                            <div class="row" id="productList">
                                <div class="col-xs-4 col-sm-4 col-md-4 mt-2">
                                    <strong>Package</strong>
                                    <select name="package_id" id="package_id" class="form-control" required>
                                        <option value="">Select Package</option>

                                        @foreach ($packages as $package)
                                            <option value="{{ $package->id }}">{{ $package->package_id }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 mt-2">

                                    <strong>People:</strong>
                                    <select name="people_id" id="people_id" class="form-control" required>
                                        <option value="">Select People</option>
                                        @foreach ($peoples as $people)
                                            <option value="{{ $people->id }}">{{ $people->people_id}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 mt-2">
                                    <strong>Assigned By:</strong>
                                    <select name="unit[]" id="productWeight" class="form-control" required>
                                        <option value="">Select unit</option>
                                        @foreach ($user as $product)
                                            <option value="{{ $product->id }}">{{ $product->unit }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class=" col-sm-2 col-md-2 mt-2">
                                    <button type="submit" id="addProduct" class="btn btn-primary mt-3 ">Add</button>

                                </div>

                            </div>

                        </div>


                    </form>
                </div>
                <!--end row-->
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection
