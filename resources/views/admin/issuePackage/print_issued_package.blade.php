@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }

    .print-only{
        display: none;
    }
    @media print {
            .print-only {
                display: block !important;
            }
        }
</style>
@section('content')
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Issue Package</div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-sm float-end" onclick="PrintElem('mainSection')">Print</button>
                    </div>
                </div>
                <div class="row" id="mainSection">

                    <h3>Admin Copy</h3>
                    <strong class="mb-3" style="margin-top: 10px;">Issued Package Details</strong>
                    <div class="row" id="deservingRow">

                       <table class="table table-hover" id="package-products">
                            <tr>
                            <td>
                                <div class="col-md-4">
                                <label for="package_name">Name:</label>
                                <p id="package_name">{{ $people->name }}</p>
                                </div>
                            </td>
                            <td>
                                <div class="col-md-4">
                                    <label for="package_desc">CNIC:</label>
                                    <p id="package_desc">{{ $people->cnic }}</p>
                                </div>
                            </td>

                            </tr>
                            <tr>
                                <td>
                                <div class="col-md-4">
                                <label for="package_name">Package Name:</label>
                                <p id="package_name">{{ $package->package_name }}</p>
                                </div>
                                </td>
                                <td>
                                <div class="col-md-4">
                                    <label for="package_desc">Issue Date:</label>
                                    <p id="package_desc">{{ $issuedPackage->created_at }}</p>
                                </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                <div class="col-md-4">
                                <label for="package_desc">Description:</label>
                                <p id="package_desc">{{ $package->description }}</p>
                                </div>
                                </td>
                                <td>
                                <div class="col-md-4">
                                    <label for="package_desc">Issued By:</label>
                                    <p id="package_desc">{{ $issuedBy->first_name .' '.$issuedBy->last_name }}</p>
                                </div>
                                </td>


                            </tr>
                        </table>

                    </div>

                    <strong  class="mb-3">Package Products</strong>
                    <div class="row" id="pacakgeRow">
                        <div class="col-md-12">
                            <table class="table table-hover" id="package-products">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($package->packageProducts as $item)

                                    <tr>
                                        <td>{{ @$item->product->product_name }}</td>
                                        <td>{{ $item->unit }}</td>
                                        <td>{{ $item->qty }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <hr class="print-only">
                    <h3 class="print-only"  style="margin-top: 10px;">Deserving Person Copy</h3>
                    <strong class="mb-3 print-only" style="margin-top: 10px">Issued Package Details</strong>
                    <div class="row print-only" id="deservingRow">

                       <table class="table table-hover print-only" id="package-products">
                            <tr>
                            <td>
                                <div class="col-md-4 print-only">
                                <label for="package_name">Name:</label>
                                <p id="package_name">{{ $people->name }}</p>
                                </div>
                            </td>
                            <td>
                                <div class="col-md-4 print-only">
                                    <label for="package_desc">CNIC:</label>
                                    <p id="package_desc">{{ $people->cnic }}</p>
                                </div>
                            </td>

                            </tr>
                            <tr>
                                <td>
                                <div class="col-md-4 print-only">
                                <label for="package_name">Package Name:</label>
                                <p id="package_name">{{ $package->package_name }}</p>
                                </div>
                                </td>
                                <td>
                                <div class="col-md-4 print-only">
                                    <label for="package_desc">Issue Date:</label>
                                    <p id="package_desc">{{ $issuedPackage->created_at }}</p>
                                </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                <div class="col-md-4 print-only">
                                <label for="package_desc">Description:</label>
                                <p id="package_desc">{{ $package->description }}</p>
                                </div>
                                </td>
                                <td>
                                <div class="col-md-4 print-only">
                                    <label for="package_desc">Issued By:</label>
                                    <p id="package_desc">{{ $issuedBy->first_name .' '.$issuedBy->last_name }}</p>
                                </div>
                                </td>


                            </tr>
                        </table>
                    </div>

                    <strong  class="mb-3 print-only">Package Products</strong>
                    <div class="row " id="pacakgeRow">
                        <div class="col-md-12">
                            <table class="table table-hover print-only" id="package-products">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($package->packageProducts as $item)

                                    <tr>
                                        <td>{{ @$item->product->product_name }}</td>
                                        <td>{{ $item->unit }}</td>
                                        <td>{{ $item->qty }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!--end page-content-wrapper-->
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        function PrintElem(elem) {
            var mywindow = window.open('', 'PRINT', 'height=900,width=1000');

            mywindow.document.write('<html><head><title>' + document.title + '</title>');
            mywindow.document.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"></head><body >');
            mywindow.document.write('<h1>' + document.title + '</h1>');
            mywindow.document.write(document.getElementById(elem).innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
             mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            // mywindow.close();

            return true;
        }
    </script>
@endpush
