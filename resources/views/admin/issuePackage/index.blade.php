@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h3>Issued Packages</h3>
                        <a href="{{ url('admin/search-issue-people') }}" class="btn btn-primary btn-sm float-end">Issue Package</a>
                    </div>

                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">
								
                            <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>CNIC</th>
                                <th>Package Name</th>
                                <th>Issue Date</th>
                                <th>Package Description</th>
                                <th>Action</th>
                            </thead>

                            <tbody>

                                @forelse ($issuePackages as $package)
                                    <tr>
                                        <td>{{ $package->id }}</td>
                                        <td>{{ @$package->People->name }}</td>
                                        <td>{{ @$package->People->cnic }}</td>
                                        <td>{{ @$package->Package->package_name }}</td>
                                        <td>{{ @$package->Package->created_at }}</td>
                                        <td>{{ @$package->Package->description }}</td>
                                        <td>
                                            {{-- <a href="{{ url('admin/edit/package/'.$package->id) }}"
                                                class="btn btn-primary btn-sm">Edit</a> --}}
                                            <a href="{{ url('admin/issued-package/'.$package->id) }}"
                                                class="btn btn-info btn-sm">View</a>

                                            {{-- <a href="{{ url('admin/delete/package/'.$package->id) }}"
                                                onclick="return confirm('Are you sure, you want to delete this data?')"
                                                class="btn btn-danger btn-sm">Delete</a> --}}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No package Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
