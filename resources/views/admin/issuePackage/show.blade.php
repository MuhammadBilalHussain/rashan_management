@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <div class="page-wrapper">
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Issue Package</div>
                </div>
                <div class="row" id="mainSection">
                    <strong class="mb-3">Details</strong>
                    <div class="row" id="deservingRow">
                        <div class="col-md-4">
                            <label for="package_name">Name:</label>
                            <p id="package_name">{{ $people->name }}</p>
                        </div>
                        <div class="col-md-4">
                            <label for="package_desc">CNIC:</label>
                            <p id="package_desc">{{ $people->cnic }}</p>
                        </div>
                        <div class="col-md-4">
                            <label for="package_name">Package Name:</label>
                            <p id="package_name">{{ $package->package_name }}</p>
                        </div>
                        <div class="col-md-4">
                            <label for="package_desc">Last Issue Date:</label>
                            <p id="package_desc">{{ $people->assignedPackages()->latest()->first() ? $people->assignedPackages()->latest()->first()->created_at : ''  }}</p>
                        </div>
                        <div class="col-md-4">
                            <label for="package_desc">Package Description:</label>
                            <p id="package_desc">{{ $package->description }}</p>
                        </div>
                    </div>
                    <hr>

                    <strong class="mb-3">Package Products</strong>
                    <div class="row" id="pacakgeRow">
                        <div class="col-md-12">
                            <table class="table table-hover" id="package-products">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($package->packageProducts as $item)
                                   
                                        <tr>
                                            <td>{{ @$item->product->product_name }}</td>
                                            <td>{{ $item->unit }}</td>
                                            <td>{{ $item->qty }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right">
                    <form action="{{ url('admin/issue-now') }}" method="post">
                        @csrf
                        <input type="hidden" name="package" value="{{ $package->id }}" />
                        <input type="hidden" name="d_people" value="{{ $people->id }}" />
                        <button class="btn btn-secondary btn-sm">Issue Now</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
