@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Issue Package</div>
                </div>

                <!--end breadcrumb-->
                <form action="{{ url('admin/search-issue-people') }}" method="post">
                    @csrf
                    <div class="row bg-white mb-3">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" name="search_option" id="search_by_cnic"
                                        value="cnic" checked>
                                    <label class="form-check-label" for="search_by_cnic">Search via CNIC</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" name="search_option"
                                        id="search_by_fingerprint" value="fingerprint">
                                    <label class="form-check-label" for="search_by_fingerprint">Search via
                                        fingerprint</label>
                                </div>
                                <label for="d_people" class="mt-3">Enter CNIC or Fingerprint</label>
                                <input type="text" class="form-control" name="query" id="d_people"
                                    placeholder="Enter CNIC or Fingerprint" />
                            </div>
                            <div class="col-md-6" id="fingerprint-container" style="display: none;">
                                <img src="{{ asset('assets/images/icons/fingerprint-image.jpg') }}" alt="Fingerprint Image"
                                    style="height: 150px; width:150px;">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-primary mt-3" type="submit">Search</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
@endsection
@push('scripts')

    <script>
        $(document).ready(function() {
            $('#search_by_cnic').click(function() {
                $('#d_people').show();
                $('#fingerprint-container').hide();
            });

            $('#search_by_fingerprint').click(function() {
                $('#d_people').hide();
                $('#fingerprint-container').show();
            });
        });
    </script>
@endpush
