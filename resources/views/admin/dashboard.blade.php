@extends('layouts.admin')

@section('content')

<div class="page-wrapper">
    <!--page-content-wrapper-->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-12 col-lg-3">
                    <div class="card radius-15 bg-voilet">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="text-white">
                                    @php
                                       $users = DB::table('users')->count();
                                        echo $users;
                                    @endphp

                                    <h2 class="mb-0 text-white">

                                        <i class='bx  font-14 text-white'></i>
                                    </h2>
                                </div>
                                <div class="ms-auto font-35 text-white"><i class="bx bx-user"></i>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-white">Users</p>
                                </div>
                                <div class="ms-auto font-14 text-white"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="card radius-15 bg-primary-blue">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="text-white">

                                    {{-- // $query = 'select count(*) as total from rd_people';
                                    // $result = mysqli_query($conn, $query);
                                    // $fetch = mysqli_fetch_array($result);
                                    // $countrPerson = $fetch['total']; --}}
                                    @php
                                       $people = DB::table('people')->count();
                                        echo $people;
                                    @endphp


                                    <h2 class="mb-0 text-white">

                                        <i class='bx  font-14 text-white'></i>
                                    </h2>
                                </div>

                                <div class="ms-auto font-35 text-white"><i class="bx bx-user"></i>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <div>
                                    <a href="persons.php">
                                        <p class="mb-0 text-white">Total Persons</p>
                                    </a>
                                </div>
                                <div class="ms-auto font-14 text-white"></div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="card radius-15 bg-rose">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="text-white">

                                    {{-- // $query = 'select count(*) as total from rd_packages';
                                    // $result = mysqli_query($conn, $query);
                                    // $fetch = mysqli_fetch_array($result);
                                    // $countrPackage = $fetch['total']; --}}
                                    @php
                                        $packages = DB::table('packages')->count();
                                        echo $packages;
                                    @endphp


                                    <h2 class="mb-0 text-white"> <i
                                            class='bx bxs-up-arrow-alt font-14 text-white'></i> </h2>
                                </div>
                                <div class="ms-auto font-35 text-white"><i class="bx bx-tachometer"></i>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0 text-white">Total Packages</p>
                                </div>
                                <div class="ms-auto font-14 text-white"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="card radius-15 bg-sunset">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="text-white">

                                    {{-- // $query = 'select count(*) as total from rd_issued_package';
                                    // $result = mysqli_query($conn, $query);
                                    // $fetch = mysqli_fetch_array($result);
                                    // $issuedPackage = $fetch['total']; --}}
                                    @php
                                        $issuePkg = DB::table('packages')->count();
                                        echo $issuePkg;
                                    @endphp
                                    <h2 class="mb-0 text-white"> <i
                                            class='bx bxs-up-arrow-alt font-14 text-white'></i> </h2>
                                </div>
                                <div class="ms-auto font-35 text-white"><i class="bx bx-user"></i>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <div>
                                    <a href="issuedpackages.php">
                                        <p class="mb-0 text-white">Total Issued</p>
                                    </a>
                                </div>
                                <div class="ms-auto font-14 text-white"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->
            <div class="card radius-15">
                <div class="card-header border-bottom-0">
                    <div class="d-lg-flex align-items-center">
                        <div>
                            <h5 class="mb-2 mb-lg-0">Donations Issued Month wise</h5>
                        </div>
                        <div class="ms-lg-auto mb-2 mb-lg-0">
                            <div class="btn-group-round">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-white">Daily</button>
                                    <button type="button" class="btn btn-white">Weekly</button>
                                    <button type="button" class="btn btn-white">Monthly</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div id="chart1"></div>
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end page-content-wrapper-->
</div>



@endsection

@push('scripts')

<script src="{{ asset('assets/js/index2.js') }}"></script>
    <script src="{{ asset('assets/plugins/apexcharts-bundle/js/apexcharts.min.js') }}"></script>

<script>
    
    // Function to fetch and update chart data
function updateChart() {
    $.ajax({
        url: 'your_json_endpoint_url_here', // Replace with your JSON data endpoint
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            // Assuming your JSON data has the same structure as the 'options' variable
            options.series[0].data = data.income;
            options.series[1].data = data.expenses;
            
            // Update the chart with new data
            var chart = new ApexCharts(document.querySelector("#chart"), options);
            chart.render();
        },
        error: function (xhr, status, error) {
            console.error('Error fetching JSON data:', error);
        }
    });
}

// Call the function to initially load the chart
updateChart();
</script>
@endpush
