@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">
                
                <div class="card">
                    <div class="card-header">
                        <h3>User Listing</h3>
                        {{-- <a href="{{ route('admin.donar.create') }}" class="btn btn-primary btn-sm float-end">Add Donar</a> --}}
                    </div>

                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">
								
                            <thead>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Status</th>

                                <th> Action</th>
                            </thead>

                            <tbody>

                                @forelse ($userslistings as $userslisting)
                                    <tr>
                                        <td>{{ $userslisting->id }}</td>

                                        <td>{{ $userslisting->first_name }}</td>
                                        <td>{{ $userslisting->last_name }}</td>
                                        <td>{{ $userslisting->email }}</td>
                                        <td>
                                            <form
                                                action="{{ route('admin.userlist.updateUserstatus', ['id' => @$userslisting->id]) }}"
                                                method="POST">
                                                @csrf
                                                @method('PATCH')
                                                <input type="hidden" name="status"
                                                    value="{{ @$userslisting->status === 1 ? 0 : 1 }}">
                                                <button type="submit"
                                                    class="btn btn-sm {{ $userslisting->status === 1 ? 'btn-primary' : 'btn-danger' }}">

                                                    @if (@$userslisting->status === 1)
                                                        {{ 'Active' }}
                                                    @elseif (@$userslisting->status === 0)
                                                        {{ 'Inactive' }}
                                                    @endif
                                                </button>
                                            </form>
                                        </td>
                                        <td>
                                            <a href="{{ url('userlist/destroy', $userslisting->id) }}"
                                                onclick="return confirm('Are you sure, you want to delete this data?')"
                                                class="btn btn-danger btn-sm">Delete</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No userslisting Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div></div>
                </div>
            </div>
        </div>
    </div>
@endsection
