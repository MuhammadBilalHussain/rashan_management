@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">
                @if (session('message'))
                    <div class="alert alert-success">{{ session('message') }}</div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3>Roles</h3>
                        {{-- <a href="{{ url('admin.role.create') }}" class="btn btn-primary btn-sm float-end">Add Donor</a> --}}
                    </div>

                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">
								
                            <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </thead>

                            <tbody>

                                @forelse ($roles as $role)
                                    <tr>
                                        <td>{{ $role->id }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>
                                            <a href="{{ route('admin.role.edit', $role->id) }}"
                                                class="btn btn-primary btn-sm">Edit</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Donar Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
