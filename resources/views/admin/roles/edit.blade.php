@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Edit Role</div>
                    
                    <div class="ms-auto">
                        <div class="btn-group">
                            <a href="{{ route('admin.donar.index') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row bg-white">
                    <form action="{{ route('admin.role.update', $role->id) }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col-xs- col-sm-6 col-md-6 mt-2">
                                <strong>Name:</strong>
                                <input type="text" name="name" id="name" value="{{ $role->name }}"
                                    class="form-control" />
                            </div>
                            <div class="col-md-12">
                                <strong>Permissions:</strong>
                                <select id="permission" name="permission[]" class="form-control" multiple="">
                                    @foreach ($permissions as $value)
                                        <option value="{{ $value->id }}"
                                            {{ in_array($value->id, $rolePermissions) ? 'selected' : '' }}>
                                            {{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class=" col-sm-12 col-md-12 text-end">
                            <button type="submit" class="btn btn-primary mt-2">Update</button>
                        </div>

                </div>
                </form>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#permission').select2();
        });
    </script>
@endpush
