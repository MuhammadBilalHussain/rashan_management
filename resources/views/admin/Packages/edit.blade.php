@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Edit Package</div>

                    <div class="ms-auto">
                        <div class="btn-group">
                            <a href="{{ url('admin/package') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row bg-white">
                    <form action="{{ url('admin/update/package/' . $package->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <!-- Hidden input field to store the package ID -->
                        <input type="hidden" name="package_id" value="{{ $package->id }}">
                        <div id="box">
                            <div class=" col-sm-2 col-md-2 mt-2 float-end">
                                <button type="submit" id="addProduct" class="btn btn-primary mt-3 ">Add</button>

                            </div>
                            @foreach ($pkgproducts as $key => $pkg)
                                <div class="row">

                                    <div class="col-xs-4 col-sm-4 col-md-4 mt-2">
                                        <strong>Product Name:</strong>
                                        <select name="data[{{$key}}][product_id]" id="productName" class="form-control" required>
                                            <option value="">Select Product</option>

                                            @foreach ($products as $product)
                                                <option value="{{ $product->id }}"
                                                    {{ $product->id == $pkg->product_id ? 'selected' : '' }}>
                                                    {{ strtoupper($product->product_name) }} - unit - {{ $product->unit }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 mt-2">

                                        <strong>Quantity:</strong>
                                        <input type="number" class="form-control" name="data[{{$key}}][qty]"
                                            value="{{ $pkg->qty }}" />
                                    </div>

                                    <div class=" col-sm-2 col-md-2 mt-2">
                                        <button type="submit" id="remove_row" class="btn btn-danger btn-sm mt-3 ">Remove</button>

                                    </div>

                                </div>
                            @endforeach

                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group mt-2">
                                    <strong>Package Name:</strong>
                                    <input type="text" name="package_name" class="form-control"
                                        value="{{ $package->package_name }}" placeholder="Package name">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group mt-2">
                                    <strong>Description:</strong>
                                    <input type="text" name="description" class="form-control"
                                        value="{{ $package->description }}" placeholder="description">
                                </div>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-12 text-end">
                            <button type="submit" class="btn btn-primary mt-2">Update</button>
                        </div>
                    </form>

                </div>
                <!--end row-->
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $("#addProduct").on("click", function(e) {
                e.preventDefault();
                var element = document.getElementById("box");
                var numberOfChildren = element.children.length;

                let index = numberOfChildren;
                var newTask = `
            <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 mt-2">
                                <strong>Product Name:</strong>
                                <select name="data[${index}][product_id]" id="productName" class="form-control" required>
                                    <option value="">Select Product</option>

                                    @foreach ($products as $product)
                                        <option value="{{ $product->id }}">{{ $product->product_name }} - unit - {{ $product->unit }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 mt-2">
                                <strong>Quantity:</strong>
                                <input type="number" class="form-control" name="data[${index}][qty]" />
                            </div>

                            <div class=" col-sm-2 col-md-2 mt-2">
                                <button type="submit" id="remove_row" class="btn btn-danger btn-sm mt-3 ">Remove</button>

                            </div>

                        </div>
                `;
                $("#box").append(newTask);


            });


            $(document).on("click", "#remove_row", function() {

                $(this).closest(".row").remove();
            })

        });
    </script>
@endpush
