@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Assign Package</div>


                </div>
                <!--end breadcrumb-->
                <form action="{{ url('admin/update-assigned-package/' . $assign->id) }}" method="post">
                    @csrf
                    @method('PUT')
                    <!-- Hidden input field to store the package ID -->
                    <input type="hidden" name="package_id" value="{{ $assign->id }}">
                    <div class="row bg-white mb-3">
                        <div class="row">

                            <div class="col-md-6">
                                <label for="package">Package</label>

                                <select name="package" id="updatePackage" class="form-control">
                                    <option value="">Select</option>
                                    @if (isset($packages))
                                        @foreach ($packages as $pkg)
                                            @if ($pkg->id == $assign->package_id)
                                                <option value="{{ $pkg->id }}" selected>{{ $pkg->package_name }}
                                                </option>
                                            @else
                                                <option value="{{ $pkg->id }}">{{ $pkg->package_name }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="d_people">Deserving People</label>
                                <select name="d_people" id="d_people" class="form-control myselect">
                                    <option value="">Select</option>
                                    @foreach ($d_people as $item)
                                        @if ($item->id == $assign->people_id)
                                            <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                                        @endif
                                        <option value="{{ $item->id }}">{{ $item->name }}- CNIC -{{ $item->cnic }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="row d-none" id="pacakgeRow">
                        <strong>Package Details</strong>
                        <div class="col-md-6">
                            <label for="package_name">Package Name:</label>
                            <p id="package_name"></p>
                        </div>
                        <div class="col-md-6">
                            <label for="package_desc">Package Description:</label>
                            <p id="package_desc"></p>
                        </div>
                        <div class="col-md-12 mt-4">
                            <table class="table table-hover" id="package-products">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="w-100 text-right">
                        <button class="btn btn-secondary btn-sm">Update</button>
                    </div>
                </form>
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            // alert("hello");
            $('#updatePackage').on('change', function(e) {

                let id = $(this).val();
                let newData = {
                    'id': id,
                };

                $.ajax({
                    url: "{{ url('admin/get-package-detail') }}",
                    type: 'GET',
                    data: {
                        '_token': "{{ csrf_token() }}",
                        'id': id,

                    },
                    success: function(response) {
                        let package = response.data;
                        $('#pacakgeRow').removeClass('d-none');
                        $('#package_name').text(package.package_name);
                        $('#package_desc').text(package.description);
                        package.package_products.forEach(i => {
                            $('#package-products tbody').append(`<tr>
                            <td>${i.product.product_name}</td>
                            <td>${i.product.unit}</td>
                            <td>${i.qty}</td>
                        </tr>`);
                        })

                        console.log(response)
                    }

                });
            });
        });
    </script>
@endpush
