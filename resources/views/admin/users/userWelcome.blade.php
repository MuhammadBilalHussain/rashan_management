
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">
                @if (session('message'))
                    <div class="alert alert-success">{{ session('message') }}</div>
                @endif
                <div class="card">
                    <h3>Welcome! <span>dear user</span></h3>
                    <h4>You have logged in ! your request is pending for the admin approval</h4>
                </div>
            </div>
        </div>
    </div>
