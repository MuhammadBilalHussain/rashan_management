@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Add New User</div>

                    <div class="ms-auto">
                        <div class="btn-group">
                            <a href="{{ route('admin.donar.index') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row bg-white">
                    <form action="{{ route('admin.donar.store') }}" method="POST">
                        @csrf

                            <div class="row">

                                <div class="col-xs- col-sm-6 col-md-6 mt-2">
                                    <strong>First Name:</strong>
                                    <input type="text" name="first_name" id="name" value="{{old('first_name')}}" class="form-control" />
                                </div>

                                <div class="col-xs- col-sm-6 col-md-6 mt-2">
                                    <strong>Last Name:</strong>
                                    <input type="text" name="last_name" id="name" value="{{old('last_name')}}" class="form-control" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs- col-sm-6 col-md-6 mt-2">

                                    <strong>Email:</strong>
                                    <input type="email" class="form-control" name="email" value="{{old('email')}}" class="form-control"/>
                                </div>


                                <div class="col-xs- col-sm-6 col-md-6 mt-2">

                                    <strong>Password:</strong>
                                    <input type="password" class="form-control" name="password" value="{{old('password')}}" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-xs- col-sm-6 col-md-6 mt-2">
                                <strong>Select Role:</strong>
                                    <select name="role_id" id="role_id" class="form-control">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                            </div>

                                <div class=" col-sm-12 col-md-12 text-end">
                                <button type="submit" class="btn btn-primary mt-2">Submit</button>
                                </div>

                        </div>
                    </form>
                </div>
                <!--end row-->
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection

