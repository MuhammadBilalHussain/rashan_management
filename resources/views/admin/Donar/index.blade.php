@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">
                @if (session('message'))
                    <div class="alert alert-success">{{ session('message') }}</div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3>Users</h3>
                        <a href="{{ route('admin.user.create') }}" class="btn btn-primary btn-sm float-end">Add Users</a>
                    </div>

                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">
								
                            <thead>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Status</th>

                                <th> Action</th>
                            </thead>

                            <tbody>

                                @forelse ($donars as $donar)
                                    <tr>
                                        <td>{{ $donar->id }}</td>

                                        <td>{{ $donar->first_name }}</td>
                                        <td>{{ $donar->last_name }}</td>
                                        <td>{{ $donar->email }}</td>
                                        <td>
                                            @foreach($donar->roles as $role)
                                                {{ $role->name }}
                                                @if(!$loop->last)

                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @if($donar->status === 1)
                                            <span class="text-primary">Active</span>
                                            @else
                                            <span class="text-danger">Inactive</span>
                                            @endif
                                        </td>
                                      <td>
                                        <a href="{{ route('admin.user.edit' ,$donar->id) }}"
                                            class="btn btn-primary btn-sm">Edit</a>

                                            <a href="{{ url('admin/delete-user',$donar->id) }}"
                                                onclick="return confirm('Are you sure, you want to delete this data?')"
                                                class="btn btn-danger btn-sm">Delete</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Donor Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
