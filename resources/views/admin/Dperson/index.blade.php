@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>
    <!-----Pop-up---->
    <div class="modal fade" id="fingerprintModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Your Fingerprint</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Your fingerprint image and message here -->
                <img src="{{asset('assets/images/icons/fingerprint-image.png')}}" alt="Fingerprint" style="max-width: 100%;">
                <p>Please add your fingerprint here...</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="cancel" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Donation Receiving Person</h3>
                        
                        <a href="#" class="btn btn-info btn-sm float-end" id="addFingerprint">Add Fingerprint</a>

                        <a href="{{ url('admin/add/person') }}" class="btn btn-primary btn-sm float-end">Add Person</a>
                    </div>

                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">
								
                            <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Father Name</th>
                                <th>Gender</th>
                                <th>CNIC</th>
                                <th>Martial Status</th>
                                <th>Image</th>
                                <th>Mobile</th>
                                <th>Address</th>
                                {{-- <th>city</th>
                                <th>State</th> --}}
                                <th style="width: 15%"> Action</th>
                            </thead>
                            <tbody>
                                @forelse ($dpersons as $dperson)
                                    <tr>
                                        <td>{{ $dperson->id }}</td>
                                        <td>{{ $dperson->name }}</td>
                                        <td>{{ $dperson->father_name }}</td>
                                        <td>{{ $dperson->gender }}</td>
                                        <td>{{ $dperson->cnic }}</td>
                                        <td>{{ $dperson->martial_status }}</td>
                                       
                                        <td>
                                            <img src="{{ asset('../images/' . $dperson->image) }}
                                            " style="max-width: 100px;">
                                        </td>
                                        <td>{{ $dperson->mobile }}</td>
                                        <td>{{ $dperson->address }}</td>
                                        {{-- <td>{{ $dperson->city->name }}</td>
                                        <td>{{ $dperson->state->name }}</td> --}}
                                        <td>
                                            <a href="{{ url('admin/edit/person/' . $dperson->id) }}"
                                                class="btn btn-primary btn-sm">Edit</a>

                                            <a href="{{ url('admin/delete/person/' . $dperson->id) }}"
                                                onclick="return confirm('Are you sure, you want to delete this data?')"
                                                class="btn btn-danger btn-sm">Delete</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Person Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

<script>
    $(document).ready(function () {
        
        $("#addFingerprint").click(function () {
            $("#fingerprintModal").modal("show");
            $("#cancel").modal("hide");
        });
    });
</script>
    
@endpush