@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')

    <!--page-wrapper-->
		<div class="page-wrapper">
			<!--page-content-wrapper-->
			<div class="page-content-wrapper">
				<div class="page-content">
					<!--breadcrumb-->
					<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
						<div class="breadcrumb-title pe-3">Edit Deserving Person's data</div>
						 
						<div class="ms-auto">
							<div class="btn-group">
                                <a class="btn btn-primary btn-sm float-end" href="{{ url('admin/dperson') }}"> Back</a>
							</div>
						</div>
					</div>
					<!--end breadcrumb-->
					<div class="row bg-white">
                        <form action="{{ url('admin/update/person/'.$person->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Image</strong>
                                    <input type="file" class="form-control" id="image" name="image" >
                                    <img  src="{{url('../images/')}}/{{$person->image }}" style="width: 150px">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Name:</strong>
                                        <input type="text" name="name" class="form-control" value="{{$person->name}}"
                                        placeholder="Name" required>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Father Name:</strong>
                                        <input type="text" name="father_name" class="form-control" value="{{$person->father_name}}"
                                        placeholder="father_name" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group mt-2">
                                        <strong>Gender:</strong>
                                       <select id="gender" name="gender" class="form-control">
                                            <option value="male" {{ $person->gender === 'male' ? 'selected' : '' }}>Male</option>
                                            <option value="female" {{ $person->gender === 'female' ? 'selected' : '' }}>Female</option>
                                            <option value="other" {{ $person->gender === 'other' ? 'selected' : '' }}>Other</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group mt-2">
                                        <strong>Martial Status:</strong>
                                    <select id="martial_status" name="martial_status" class="form-control">
                                        <option value="single" {{ $person->martial_status === 'single' ? 'selected' : '' }}>Single</option>
                                        <option value="married" {{ $person->martial_status === 'married' ? 'selected' : '' }}>Married</option>
                                        <option value="divorced" {{ $person->martial_status === 'divorced' ? 'selected' : '' }}>Divorced</option>
                                        <option value="widowed" {{ $person->martial_status === 'widowed' ? 'selected' : '' }}>Widowed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>CNIC:</strong>
                                        <input type="number" name="cnic" class="form-control"  value="{{$person->cnic}}"
                                        placeholder="CNIC" required>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Date of birth:</strong>
                                        <input type="date" name="dob" class="form-control" value="{{$person->dob}}"
                                        placeholder="Date of birth" required>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Mobile:</strong>
                                        <input type="number" name="mobile" class="form-control" value="{{$person->mobile}}"
                                        placeholder="phone" required>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong>Address:</strong>
                                        <input type="address" name="address" class="form-control" value="{{$person->address}}"
                                        placeholder="Address" required>
                                    </div>
                                </div>
                            </div>
                           <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <strong for="state_id">State</strong>
                                        <select class="form-control" id="state_id" name="state_id">
                                            @foreach ($states as $state)
                                                <option value="{{ $state->id }}" {{ $person->state_id == $state->id ? 'selected' : '' }}>{{ $state->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="city_id">City</label>
                                        <select class="form-control" id="city_id" name="city_id">
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}" {{ $person->city_id == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                           </div>

                            <div class=" col-sm-12 col-md-12 text-end">
                                <button type="submit" class="btn btn-primary mt-2">Update</button>
                            </div>


                        </form>
					</div>
					<!--end row-->
				</div>
			</div>
			<!--end page-content-wrapper-->
		</div>
		<!--end page-wrapper-->
@endsection

@push('scripts')
        <script>
            $(document).ready(function () {
                $('#state_id').change(function () {
                    var stateId = $(this).val();
                    if (stateId) {
                        $.ajax({
                            url: "/admin/get-cities/" + stateId,
                            type: 'GET',
                            dataType: 'json',
                            success: function (data) {
                                $('#city_id').empty();
                                $.each(data, function (key, value) {
                                    $('#city_id').append('<option value="' + key + '">' + value + '</option>');
                                });
                            }
                        });
                    } else {
                        $('#city_id').empty();
                    }
                });
            });
        </script>
    @endpush
