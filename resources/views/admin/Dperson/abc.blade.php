<!--end breadcrumb-->
<div class="row">
    <div class="col-xl-7 mx-auto">
        <h6 class="mb-0 text-uppercase">Basic Form</h6>
        <hr>
        <div class="card border-top border-0 border-4 border-primary">
            <div class="card-body p-5">
                <div class="card-title d-flex align-items-center">
                    <div><i class="bx bxs-user me-1 font-22 text-primary"></i>
                    </div>
                    <h5 class="mb-0 text-primary">Deserving Person Registration</h5>
                </div>
                <div class="ms-auto">
                        <div class="btn-group">
                            <a href="{{ url('admin/dperson') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                <hr>
                <form class="row g-3" action="{{ url('admin/store/person') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="col-md-6">
                        <label for="inputFirstName" class="form-label">Image</label>
            
                        <input type="file" class="form-control" id="image" name="image" >
                    </div>
                    <div class="col-md-6">
                        <label for="inputLastName" class="form-label">Name</label>
                        <!-- <input type="password" class="form-control" id="inputLastName"> -->
                        <input type="text" name="name" class="form-control" placeholder="Name" value="{{old('name')}}">
                    </div>
                    <div class="col-md-6">
                        <label for="inputEmail" class="form-label">Father Name</label>
                        
                        <input type="text" name="father_name" value="{{old('father_name')}}" class="form-control" placeholder="father_name">
            
                    </div>
                    <div class="col-md-6">
                        <label for="inputPassword" class="form-label">Gender</label>			
                        <select name="gender" class="form-control">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="other">Other</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="inputAddress" class="form-label">Martial Status:</label>
                        <select name="martial_status" class="form-control">
                            <option value="single">Single</option>
                            <option value="married">Married</option>
                            <option value="divorced">Divorced</option>
                            <option value="widowed">Widowed</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="inputAddress2" class="form-label">CNIC</label>
                        <input type="number" name="cnic" value="{{old('cnic')}}" class="form-control" placeholder="CNIC">
                    </div>
                    <div class="col-md-6">
                        <label for="inputCity" class="form-label">Date of Birth</label>
                        <input type="date" name="dob" value="{{old('dob')}}" class="form-control" placeholder="Date of birth">
                    </div>
                    <div class="col-md-6">
                        <label for="inputState" class="form-label">Mobile</label>
                        <input type="number" name="mobile" value="{{old('mobile')}}" class="form-control" placeholder="phone">
            
                    </div>
                    <div class="col-md-6">
                        <label for="inputZip" class="form-label">Address</label>
                        <input type="address" name="address" value="{{old('address')}}" class="form-control" placeholder="Address">
            
                    </div>
                    <div class="col-md-6">
                        
                            <label class="form-check-label" for="gridCheck">Province</label>
                            <select class="form-control" id="state_id" name="state_id">
                            <option value="">Select State</option>
                            @foreach ($states as $state)
                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        
                        <label for="city_id">City</label>
                        <select class="form-control" id="city_id" name="city_id">
                            <option value="">Select City</option>

                            {{-- <option value="{{ $state->id }}">{{ $city->name }}</option> --}}
                        </select>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary px-5">Register</button>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
</div>
<!--end row-->