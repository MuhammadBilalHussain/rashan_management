@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Deserving Person's data</div>

                    <div class="ms-auto">
                        <div class="btn-group">
                            <a href="{{ url('admin/dperson') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row bg-white">
                    <form class="row g-3" action="{{ url('admin/store/person') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <strong>Image</strong>
                                <input type="file" class="form-control" id="image" name="image" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Name:</strong>
                                    <input type="text" name="name" class="form-control" placeholder="Name" value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Father Name:</strong>
                                    <input type="text" name="father_name" value="{{old('father_name')}}" class="form-control" placeholder="father_name">
                               
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Gender:</strong>
                                    <select name="gender" class="form-control">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Martial Status:</strong>
                                    <select name="martial_status" class="form-control">
                                        <option value="single">Single</option>
                                        <option value="married">Married</option>
                                        <option value="divorced">Divorced</option>
                                        <option value="widowed">Widowed</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>CNIC:</strong>
                                    <input type="number" name="cnic" value="{{old('cnic')}}" class="form-control" placeholder="CNIC">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Date of birth:</strong>
                                    <input type="date" name="dob" value="{{old('dob')}}" class="form-control" placeholder="Date of birth">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Mobile:</strong>
                                    <input type="number" name="mobile" value="{{old('mobile')}}" class="form-control" placeholder="phone">
                                
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Address:</strong>
                                    <input type="address" name="address" value="{{old('address')}}" class="form-control" placeholder="Address">
                                
                                </div>
                            </div>
                        </div>
                       <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong for="state_id">State</strong>
                                    <select class="form-control" id="state_id" name="state_id">
                                        <option value="">Select State</option>
                                        @foreach ($states as $state)
                                            <option value="{{ $state->id }}">{{ $state->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="city_id">City</label>
                                    <select class="form-control" id="city_id" name="city_id">
                                        <option value="">Select City</option>

                                        {{-- <option value="{{ $state->id }}">{{ $city->name }}</option> --}}
                                    </select>
                                </div>
                            </div>
                       </div>

                        <div class=" col-sm-12 col-md-12 text-end">
                            <button type="submit" class="btn btn-primary mt-2">Submit</button>
                        </div>


                    </form>
                </div>
                <!--end row-->
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
    $('#state_id').change(function () {
        var stateId = $(this).val();
        // alert(stateId);
        if (stateId) {
            $.ajax({
                url: "/admin/get-cities/"+stateId, // Define a route to fetch cities based on the state
                type: 'GET',
                // _token: '{{csrf_token()}}'
                dataType: 'json',
                success: function (data) {
                    $('#city_id').empty();
                    $.each(data, function (key, value) {
                        $('#city_id').append('<option value="' + key + '">' + value + '</option>');
                    });
                }
            });
        } else {
            $('#city_id').empty();
        }
    });
});
</script>
@endpush
