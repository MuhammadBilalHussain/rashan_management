@extends('layouts.admin')
<style>
    .container {
        margin-top: 80px;
        margin-left: 20%;
        /* margin-left: 10%; */
    }

    .col-md-10 {
        margin-left: 10%;
    }
</style>
@section('content')
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="page-content">
                <!--breadcrumb-->
                <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                    <div class="breadcrumb-title pe-3">Add New Stock</div>
                    
                    <div class="ms-auto">
                        <div class="btn-group">
                            <a href="{{ url('admin/package') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <!--end breadcrumb-->
                <div class="row bg-white">
                    <form action="{{ route('admin.stock.store') }}" method="POST">
                        @csrf
                        <div id="box" >
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 mt-2">
                                    <strong>Product Name:</strong>
                                    <select name="data[0][product_id]" id="productName" class="form-control" required>
                                        <option value="">Select Product</option>

                                        @foreach ($products as $product)
                                            <option value="{{ $product->id }}">{{ strtoupper($product->product_name ) }} - unit - {{ $product->unit }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 mt-2">

                                    <strong>Quantity:</strong>
                                    <input type="number" class="form-control" name="data[0][qty]" />
                                </div>

                                <div class=" col-sm-2 col-md-2 mt-2">
                                    <button type="submit" id="addProduct" class="btn btn-primary mt-3 ">Add</button>

                                </div>

                            </div>

                        </div>

                        <div class=" col-sm-12 col-md-12 text-end">
                            <button type="submit" class="btn btn-primary mt-2">Submit</button>
                        </div>
                    </form>
                </div>
                <!--end row-->
            </div>
        </div>
        <!--end page-content-wrapper-->
    </div>
    <!--end page-wrapper-->
@endsection

@push('scripts')

<script>
    $(document).ready(function() {
        $("#addProduct").on("click", function(e) {
            e.preventDefault();
            var element = document.getElementById("box");
            var numberOfChildren = element.children.length;

            let index = numberOfChildren;
            var newTask = `
            <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 mt-2">
                                <strong>Product Name:</strong>
                                <select name="data[${index}][product_id]" id="productName" class="form-control" required>
                                    <option value="">Select Product</option>

                                    @foreach ($products as $product)
                                        <option value="{{ $product->id }}">{{ $product->product_name }} - unit - {{ $product->unit }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 mt-2">
                                <strong>Quantity:</strong>
                                <input type="number" class="form-control" name="data[${index}][qty]" />
                            </div>

                            <div class=" col-sm-2 col-md-2 mt-2">
                                <button type="submit" id="remove_row" class="btn btn-danger btn-sm mt-3 ">Remove</button>

                            </div>

                        </div>
                `;
            $("#box").append(newTask);


        });


        $(document).on("click", "#remove_row", function() {

            $(this).closest(".row").remove();
        })

    });
</script>
@endpush
