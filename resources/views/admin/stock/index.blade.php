@extends('layouts.admin')

@section('content')
    <style>
        .card {
            margin-top: 80px;
            margin: 10px;
        }
    </style>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.4.1/css/buttons.dataTables.min.css">
    <!--page-wrapper-->
    <div class="page-wrapper">
        <!--page-content-wrapper-->
        <div class="page-content-wrapper">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4>Inventory Stock History</h4>
                    </div>

                    <div class="card-body">
                    <div class="table-responsive">
							<table id="example2" class="table table-striped table-bordered" style="width:100%">

                            <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Added On</th>
                                <th>Added By</th>
                            </thead>

                            <tbody>

                                @forelse ($stocks as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ @$product->product->product_name }}</td>
                                        <td>{{ $product->qty }}</td>
                                        <td>{{ $product->created_at }}</td>
                                        <td>{{ @$product->AddedBy->first_name.' '.  @$product->AddedBy->last_name}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Date Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
