
<!--sidebar-wrapper-->
<div class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        <div class="">
            <img src="../assets/images/logo-icon.png" class="logo-icon-2" alt="" />
        </div>
        <div>
            <h4 class="logo-text">Khidmat-e-Khalaq</h4>
        </div>
        <a href="javascript:;" class="toggle-btn ms-auto"> <i class="bx bx-menu"></i>
        </a>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
        @can('dashboard')
            <li>
                <a href="{{ url('admin/dashboard') }}">
                    <div class="parent-icon icon-color-1"><i class="bx bx-home-alt"></i>
                    </div>
                    <div class="menu-title">Dashboard</div>
                </a>

            </li>
        @endcan
        <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon icon-color-10"><i class="bx bx-user"></i>
                </div>
                <div class="menu-title">Users / Donors </div>
            </a>
            <ul>



        @can('users')
            <li>
                <a href="{{ route('admin.donar.index') }}">
                    <div class="parent-icon icon-color-2"><i class="bx bx-user-circle"></i>
                    </div>
                    <div class="menu-title">Users List</div>
                </a>
            </li>
        @endcan
        @can('donor')
            <li>
                <a href="{{ url('admin/donor-list') }}">
                    <div class="parent-icon icon-color-2"> <i class="bx bx-donate-blood"></i>
                    </div>
                    <div class="menu-title">Donors</div>
                </a>
            </li>
        @endcan
        @can('user-for-approvel')
            <li>
                <a href="{{ url('admin/userlist') }}">
                    <div class="parent-icon icon-color-2"> <i class="bx bx-user"></i>
                    </div>
                    <div class="menu-title">Activate / Deactivate Users</div>
                </a>
            </li>
        @endcan

        </ul>
    </li>
          <li>
            <a class="has-arrow" href="javascript:;">
                <div class="parent-icon icon-color-10"><i class="bx bx-store alt icon"></i>
                </div>
                <div class="menu-title">Inventory Details</div>
            </a>
            <ul>
                @can('inventory')
                <li>
                    <a href="{{ route('admin.product.index') }}">
                        <div class="parent-icon icon-color-3"><i class="bx bx-folder"></i>
                        </div>
                        <div class="menu-title">Add Inventory</div>
                    </a>
                </li>
                @endcan
                @can('inventory-history')
                <li>
                    <a href="{{ route('admin.product.inventory_report') }}">
                        <div class="parent-icon icon-color-3"><i class="bx bx-file"></i>
                        </div>
                        <div class="menu-title">Inventory Report</div>
                    </a>
                </li>
                @endcan
                 {{-- @can('inventory-stock-history') --}}
                    <li>
                        <a href="{{ route('admin.stock.index') }}">
                            <div class="parent-icon icon-color-3"><i class="bx bx-file"></i>
                            </div>
                            <div class="menu-title">Inventory Stock History</div>
                        </a>
                    </li>
                {{-- @endcan --}}
            </ul>
    </li>
  <li>
    <a class="has-arrow" href="javascript:;">
        <div class="parent-icon icon-color-10"><i class="bx bx-grid-alt"></i>
        </div>
        <div class="menu-title">Deserving Person</div>
    </a>
    <ul>

        @can('package')
            <li>
                <a href="{{ url('admin/package') }}">
                    <div class="parent-icon icon-color-2"><i class="bx bx-archive"></i>
                    </div>
                    <div class="menu-title">Create Packages</div>
                </a>
            </li>
        @endcan
        @can('people')
            <li>
                <a href="{{ url('admin/dperson') }}">
                    <div class="parent-icon icon-color-2"><i class="bx bx-group"></i>
                    </div>
                    <div class="menu-title">Add Person</div>
                </a>
            </li>
        @endcan

        @can('assign-package')
            <li>
                <a href="{{ url('admin/all-assign-package') }}">
                    <div class="parent-icon icon-color-3"> <i class="bx bx-repeat"></i>
                    </div>
                    <div class="menu-title">Assign Packages</div>
                </a>
            </li>
        @endcan

        @can('issue-package')
            <li>
                <a href="{{ url('admin/issue-package') }}">
                    <div class="parent-icon icon-color-3"> <i class="bx bx-task"></i>
                    </div>
                    <div class="menu-title">Issue Packages</div>
                </a>
            </li>
        @endcan

        </ul>
</li>
<li>
    <a class="has-arrow" href="javascript:;">
        <div class="parent-icon icon-color-10"><i class="bx bx-line-chart"></i>
        </div>
        <div class="menu-title">Reports</div>
    </a>
    <ul>

            <li>
                <a href="{{ url('admin/issue-packages-report') }}">
                    <div class="parent-icon icon-color-3"> <i class="bx bx-file"></i>
                    </div>
                    <div class="menu-title">Issued Packages Report</div>
                </a>
            </li>

            </ul>
    </li>
    <li>
        <a class="has-arrow" href="javascript:;">
            <div class="parent-icon icon-color-10"><i class="bx bx-cog"></i>
            </div>
            <div class="menu-title">Settings</div>
        </a>
        <ul>
        @can('roles')
            <li>
                <a href="{{ route('admin.role.index') }}">
                    <div class="parent-icon icon-color-3"> <i class='bx bx-plug me-2'></i>
                    </div>
                    <div class="menu-title">Roles</div>
                </a>
            </li>
        @endcan
        </ul>
    </li>

    </ul>
    <!--end navigation-->
</div>
<!--end sidebar-wrapper-->
