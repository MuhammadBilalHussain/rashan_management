<?php

use App\Models\IssuePackage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StockController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\DonarController;
use App\Http\Controllers\IssuePackageController;
use App\Http\Controllers\Admin\DpersonController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\InventoryController;
use App\Http\Controllers\Admin\AssignPackageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/login', function () {
    return view('auth.login');
})->name('login');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::middleware(['auth',])->group(function () {



        //Admin Routes
        Route::get('dashboard', [AdminController::class, 'AdminDashboard']);

        //DRP's Routes
        Route::get('dperson', [DpersonController::class, 'index']);
        Route::get('add/person', [DpersonController::class, 'create']);
        Route::post('store/person', [DpersonController::class, 'store']);
        Route::get('edit/person/{id}', [DpersonController::class, 'edit']);
        Route::put('update/person/{id}', [DpersonController::class, 'update']);
        Route::get('delete/person/{id}', [DpersonController::class, 'destroy']);

        // Packages routes

        Route::get('package', [PackageController::class, 'index']);
        Route::get('add/package', [PackageController::class, 'create']);
        Route::post('store/package', [PackageController::class, 'store']);
        Route::get('edit/package/{id}', [PackageController::class, 'edit']);
        Route::put('update/package/{id}', [PackageController::class, 'update']);
        Route::get('delete/package/{id}', [PackageController::class, 'destroy']);

        // inventory routes

        Route::resource('product', InventoryController::class);
        Route::get('inventory-report', [InventoryController::class,'inventory_report'])->name('product.inventory_report');

        // Donar routes

        Route::resource('donar', DonarController::class);
        Route::resource('user', DonarController::class);
        Route::get('delete-user/{id}', [DonarController::class,'destroy']);
        Route::get('delete-donor/{id}', [DonarController::class,'destroyDonor']);
        Route::get('donar-history/{id}', [DonarController::class,'donorHistory']);

        Route::post('store-donor', [DonarController::class,'storeDonor']);
        Route::get('donor-list', [DonarController::class,'donorall']);
        Route::get('donor-create', [DonarController::class,'createDonor']);
        Route::get('donor-edit/{id}', [DonarController::class,'editDonor']);
        Route::put('donar-update/{id}', [DonarController::class,'updateDonor']);
        Route::get('donar-history/{id}', [DonarController::class,'donorHistory']);
        Route::post('new-donation', [DonarController::class,'newDonation']);

       // cities route
       Route::get('/get-cities/{state}', [CityController::class, 'getCities']);
       // Roles
        Route::resource('role', RoleController::class);

        // Stock Management
        Route::resource('stock', StockController::class);



        // Assign packages
        Route::get('all-assign-package', [PackageController::class, 'allAssignPackage']);
        Route::post('assign-package', [PackageController::class, 'assignPackageNow']);
        Route::get('assign-package', [PackageController::class, 'assignPackage']);
        Route::get('get-package-detail', [PackageController::class, 'PackageDetails']);
        Route::get('edit-package-detail/{id}', [PackageController::class, 'editPackageDetails']);
        Route::get('edit-assign-package/{id}', [PackageController::class, 'editAssignPackage']);
        Route::put('update-assigned-package/{id}', [PackageController::class, 'updatePackageDetails']);
        Route::get('delete-assigned-package/{id}', [PackageController::class, 'destroyAssignedPackage']);

        // Issue Package
        Route::get('issue-package', [IssuePackageController::class, 'index']);
        Route::post('issue-now', [IssuePackageController::class, 'store']);
        Route::get('search-issue-people', [IssuePackageController::class, 'create']);
        Route::post('search-issue-people', [IssuePackageController::class, 'searchDPeople']);
        Route::get('issue-packages-report', [IssuePackageController::class, 'issuePackageReport']);
        Route::get('issued-package/{id}', [IssuePackageController::class, 'issuedPackage']);

        // Route::get('get-issuepkg/{slug}', IssuePackageController::class, 'getIssuePkg');

    });
            //Is Admin Middleware
    Route::middleware(['auth','isAdmin'])->group(function () {

        //UserListing
        Route::get('userlist',[AdminController::class, 'UsersListing']);
        Route::patch('updateStatus',[ AdminController::class,'updateUserStatus'])->name('userlist.updateUserstatus');
        Route::get('userlist/destroy/{id}',[AdminController::class, 'destroy']);

    });


});
